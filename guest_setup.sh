#!/bin/bash

sudo groupadd guest
sudo useradd -s /bin/bash -m -c guest -gguest guest
sudo passwd -d guest
sudo chmod +x guest_account.sh
sudo cp guest_account.sh /usr/bin/guest_account
sudo echo "greeter-setup-script=/usr/bin/guest_account" >> /etc/lightdm/lightdm.conf
